const wrapper = require('../../../helpers/utils/wrapper');
const commandHandler = require('../repositories/commands/command_handler');
const commandModel = require('../repositories/commands/command_model');
const queryHandler = require('../repositories/queries/query_handler');
const validator = require('../utils/validator');
const { ERROR:httpError, SUCCESS:http } = require('../../../helpers/http-status/status_code');

const getListCheckout = async (req, res) => {
  const getData = async => queryHandler.getListCheckout();
  const sendResponse = async (result) => {
    result.err
      ? wrapper.response(res, "fail", result, "Get all List Checkout Cancel", httpError.NOT_FOUND)
      : wrapper.response(res, "success", result, "Get all List Checkout", http.OK);
  };
  sendResponse(await getData())
};

const getLIstCheckoutByID = async (req, res) => {
  const { id } = req.params;
  const getData = async => queryHandler.getCartById(id);
  const sendResponse = async (result) => {
    result.err
      ? wrapper.response(res, "fail", result, "Get Checkout Cancel", httpError.NOT_FOUND)
      : wrapper.response(res, "success", result, "Get Checkout", http.OK);
  };
  sendResponse(await getData())
};

const Ulasan = async (req, res) => {
  const payload = req.body;
  const validatePayload = validator.isValidPayload(payload, commandModel.ulasan);
  const postRequest = async (result) => {
    if (result.err) {
      return result;
    }
    return commandHandler.ulasan(result.data);
  };
  const sendResponse = async (result) => {
    result.err
      ? wrapper.response(res, "fail", result, "Ulasan Cancel", httpError.NOT_FOUND)
      : wrapper.response(res, "success", result, "Ulasan", http.OK);
  };
  sendResponse(await postRequest(validatePayload))
}

const ulasanDetail = async (req, res) => {
  const { id } = req.params;
  const payload = req.body;
  const postRequest = async (result, id) => {
    if (result.err) {
      return result;
    }
    return commandHandler.updateUlasan(result, id);
  };
  const sendResponse = async (result) => {
    result.err
      ? wrapper.response(res, "fail", result, "Add Ulasan Detail Cancel", httpError.NOT_FOUND)
      : wrapper.response(res, "success", result, "Add Ulasan Detail", http.OK);
  };
  sendResponse(await postRequest(payload, id))
}

const deleteCheckout = async (req, res) => {
  const { id } = req.params;
  const deleteCart = async => commandHandler.deleteDetail(id);
  const sendResponse = async (result) => {
    result.err
      ? wrapper.response(res, "fail", result, "Delete Detail Pembelian Cancel", httpError.NOT_FOUND)
      : wrapper.response(res, "success", result, "Delete Detail Pembelian", http.OK);
  };
  sendResponse(await deleteCart())
} 

module.exports = {
  getListCheckout,
  getLIstCheckoutByID,
  Ulasan,
  ulasanDetail,
  deleteCheckout,
};
