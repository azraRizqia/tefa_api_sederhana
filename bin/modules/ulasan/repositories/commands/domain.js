const Query = require('../queries/query');
const Command = require('./command');
const wrapper = require('../../../../helpers/utils/wrapper');
const logger = require('../../../../helpers/utils/logger');
const { ConflictError, InternalServerError, NotFoundError } = require('../../../../helpers/error');

class Ulasan {
  constructor(db){
    this.command = new Command(db);
    this.query = new Query(db);
  }

  async create(payload) {
    const { payload: result } = await this.command.ulasan(payload)
    return wrapper.data( result );
  }

  async update(payload, id) {
    const { payload: result } = await this.command.detail_pembelian(payload, id)
    return wrapper.data( result );
  }

  async delete(id) {
    const ulasan = await this.query.findById(id)

    if (!ulasan.data) {
      return wrapper.error(new NotFoundError("No Detail Pembelian Founds"))
    }
    const { data: result } = await this.command.deleteDetailPembelian(id);
    return wrapper.data(result);
  }
}

module.exports = Ulasan;
