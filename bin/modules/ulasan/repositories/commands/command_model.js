const joi = require('joi');

const ulasan = joi.object({
  id_produk: joi.number().integer(),
  id_user: joi.number().integer(),
  rating: joi.number().integer(),
  ulasan: joi.string().required(),
});

module.exports = {
  ulasan,
};
