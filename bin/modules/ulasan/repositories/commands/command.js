class Command {
  constructor(db) {
    this.db = db;
  }

  async ulasan(document){
    const { id_produk, id_user, rating, ulasan} = document;
    const result = await this.db.prepareQuery(
      "INSERT INTO ulasan (id_produk, id_user, rating, ulasan) VALUES (?,?,?,?)",
      [id_produk, id_user, rating, ulasan]
    );
    return result;
  }

  async detail_pembelian(document, id){
    const { rating, ulasan} = document;
    const result = await this.db.prepareQuery(
      "UPDATE detail_pembelian SET rating = ?, ulasan = ?, WHERE id = ?",
      [rating, ulasan, id]
    );
    return result;
  }

  async deleteDetailPembelian(id){
    const result = await this.db.prepareQuery(
      "DELETE FROM detail_pembelian WHERE id = ?", 
      id
    );
    const resultCheckout = await this.db.prepareQuery(
      "DELETE FROM checkout WHERE id = ?", 
      id
    );
    return result, resultCheckout;
  }
}

module.exports = Command;
