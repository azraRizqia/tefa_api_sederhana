const Ulasan = require('./domain');
const Mysql = require('../../../../helpers/databases/mysql/db');
const config = require('../../../../infra/configs/global_config');
const db = new Mysql(config.get('/mysqlConfig'));

const ulasan = async (payload) => {
  const ulasan = new Ulasan(db);
  const postCommand = async (payload) => ulasan.create(payload);
  return postCommand(payload);
};

const updateUlasan = async (payload, id) => {
  const ulasan = new Ulasan(db);
  const postCommand = async (payload, id) => ulasan.update(payload, id);
  return postCommand(payload, id);
};

const deleteDetail = async (id) => {
  const cart = new Cart(db);
  const postCommand = async (id) => cart.delete(id);
  return postCommand(id);
};

module.exports = {
  ulasan,
  updateUlasan,
  deleteDetail,
};
